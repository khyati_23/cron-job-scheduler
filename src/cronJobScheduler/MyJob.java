package cronJobScheduler;

import javax.mail.PasswordAuthentication;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import javax.mail.internet.*; 
import javax.activation.*;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session; 
import javax.mail.Transport; 

public class MyJob implements Job{

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		
		
		Calendar cal = Calendar.getInstance();
		System.out.println("TIME :: "+cal.getTime());
		
		 Properties props = new Properties();
	        props.put("mail.smtp.host", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtp.port", "587");
	        props.put("mail.smtp.auth", "true");
	        //Establishing a session with required user details
	        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication("your mail id", "your password");
	            }
	        });
	        try {
	            //Creating a Message object to set the email content
	            MimeMessage msg = new MimeMessage(session);
	            //Storing the comma seperated values to email addresses
	            String to = "send recepients mail id";
	            /*Parsing the String with defualt delimiter as a comma by marking the boolean as true and storing the email
	            addresses in an array of InternetAddress objects*/
	            InternetAddress[] address = InternetAddress.parse(to, true);
	            //Setting the recepients from the address variable
	            msg.setRecipients(Message.RecipientType.TO, address);
	            String timeStamp = new SimpleDateFormat("yyyymmdd_hh-mm-ss").format(new Date());
	            msg.setSubject("Sample Mail : " + timeStamp);
	            msg.setSentDate(new Date());
	             msg.setContent("<html>\n" +
	                    "<body>\n" +
	                    "\n" +
	                    "<label> <h1 style='color:pink'>"+cal.getTime()+"</h1> </label>\n" +
	                    "<a href=\"https://bitbucket.org/khyati_23/cron-job-scheduler/src/master\">\n" +
	                    "This is a link</a>\n" +
	                    "\n" +
	                    "</body>\n" +
	                    "</html>", "text/html");
	            //msg.setText("<h1 style='color:red'>Sampel System Generated mail</h1>");
	            msg.setHeader("XPriority", "1");
	            Transport.send(msg);
	            System.out.println("Mail has been sent successfully");
	        } catch (MessagingException mex) {
	            System.out.println("Unable to send an email" + mex);
	        }
		
	}

}
