package cronJobScheduler;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class CronJobsController {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Create instance of factory
	    SchedulerFactory schedulerFactory=new StdSchedulerFactory();

	    //Get schedular
	    Scheduler scheduler = null;
		try {
			scheduler = schedulerFactory.getScheduler();
		} catch (SchedulerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	    //Create JobDetail object specifying which Job you want to execute
	    JobDetail jobDetail=JobBuilder.newJob(MyJob.class)
	    		
                .withIdentity("job2", "group2").build();


	    //Associate Trigger to the Job
	    Trigger trigger=TriggerBuilder.newTrigger()
	    		
                .withIdentity("cronTrigger1", "group1")

                .withSchedule(CronScheduleBuilder.cronSchedule("0 0/50 * * * ?"))

                .build();


	    //Pass JobDetail and trigger dependencies to schedular
	    try {
			scheduler.scheduleJob(jobDetail,trigger);
			
			//Start schedular
		    scheduler.start();
		    
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    
		
	}

}
